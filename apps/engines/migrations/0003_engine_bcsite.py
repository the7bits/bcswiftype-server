# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2015-12-15 14:49
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bcsites', '0002_auto_20151215_1101'),
        ('engines', '0002_swiftypeoptions'),
    ]

    operations = [
        migrations.AddField(
            model_name='engine',
            name='bcsite',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='swiftype_engines', to='bcsites.BCSite', verbose_name='BC Site'),
            preserve_default=False,
        ),
    ]

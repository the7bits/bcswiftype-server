from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import BCPartner, BCSite


class BCPartnerAdmin(admin.ModelAdmin):
    list_display = (
        'partner_id', 'last_update',
        'is_access_token_expired',
        'is_refresh_token_expired')
    actions = ['refresh_token_action']

    def refresh_token_action(self, request, queryset):
        for q in queryset:
            q.refresh_access_token()
    refresh_token_action.short_description = _('Refresh access token')


class BCSiteAdmin(admin.ModelAdmin):
    list_display = ('secure_url', 'site_id', 'name', 'added')

admin.site.register(BCPartner, BCPartnerAdmin)
admin.site.register(BCSite, BCSiteAdmin)

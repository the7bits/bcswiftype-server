from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from apps.core.views import index, help


urlpatterns = [
    url(r'^', include('apps.engines.urls')),
    url(r'^', include('apps.bcsites.urls')),
    url(r'^$', index, name='index'),
    url(r'^help$', help, name="help-page"),
    url(r'^admin/', include(admin.site.urls)),
]


# This is only needed when using runserver.
if settings.DEBUG:
    from django.views.static import serve
    urlpatterns = [
        url(r'^media/(?P<path>.*)$', serve,  # NOQA
            {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
        ] + staticfiles_urlpatterns() + urlpatterns  # NOQA

    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]

from os import urandom
from base64 import b64encode, b64decode
from Crypto.Cipher import ARC4
from datetime import timedelta
from django.utils import timezone
from django.contrib.auth import get_user_model
from django.conf import settings
from .models import BCPartner, BCSite


SALT_SIZE = 8


def update_bc_sites_db(token_response):
    now = timezone.now()

    access_token = token_response.get('access_token')
    refresh_token = token_response.get('refresh_token')
    token_type = token_response.get('token_type')
    expires_in = token_response.get('expires_in')
    refresh_token_expires_in = token_response.get('refresh_token_expires_in')

    additional_info = token_response.get('additional_info')

    secure_urls = additional_info.get('secure_urls')
    partner_id = additional_info.get('partnerId')
    sites = additional_info.get('sites')

    try:
        bcpartner = BCPartner.objects.get(partner_id=partner_id)
        bcpartner.access_token = access_token
        bcpartner.refresh_token = refresh_token
        bcpartner.token_type = token_type
        bcpartner.access_token_expires = now + timedelta(seconds=expires_in)
        bcpartner.refresh_token_expires = \
            now + timedelta(seconds=refresh_token_expires_in)
        bcpartner.save()
    except BCPartner.DoesNotExist:
        bcpartner = BCPartner(
            partner_id=partner_id,
            access_token=access_token,
            refresh_token=refresh_token,
            token_type=token_type,
            access_token_expires=now + timedelta(seconds=expires_in),
            refresh_token_expires=
            now + timedelta(seconds=refresh_token_expires_in),
        )
        bcpartner.save()

    User = get_user_model()
    for site in sites:
        if site['site_id'] > 0:
            try:
                user = User.objects.get(username__exact=str(site['site_id']))
            except User.DoesNotExist:
                user = User.objects.create_user(username=str(site['site_id']))

            try:
                bcsite = BCSite.objects.get(site_id=site['site_id'])
                bcsite.name = site['name'] or 'None'
                bcsite.partner = bcpartner
                bcsite.save()
            except BCSite.DoesNotExist:
                bcsite = BCSite(
                    name=site['name'] or 'None',
                    site_id=site['site_id'],
                    user=user,
                    partner=bcpartner,
                    secure_url=site.get('secure_url'),
                )
                bcsite.save()
    return True


def get_access_token(site_id):
    try:
        bcsite = BCSite.objects.get(site_id__exact=site_id)
    except BCSite.DoesNotExist:
        return None

    bcpartner = bcsite.partner
    if bcpartner.is_access_token_expired():
        return bcpartner.refresh_access_token()
    else:
        if bcpartner.is_refresh_token_expired():
            return None
        else:
            return bcpartner.access_token


def encrypt_string(plaintext):
    salt = urandom(SALT_SIZE)
    arc4 = ARC4.new(salt + bytes(settings.ENCRYPT_SECRET_KEY, 'utf-8'))
    plaintext = "%3d%s%s" % (
        len(plaintext),
        plaintext,
        urandom(256-len(plaintext)))
    plaintext = bytes(plaintext, 'utf-8')
    return salt + b'$' + b64encode(arc4.encrypt(plaintext))


def decrypt_cipher(ciphertext):
    _t = ciphertext.split(b'$')
    salt = _t[0]
    ciphertext = b64decode(_t[1])
    arc4 = ARC4.new(salt + bytes(settings.ENCRYPT_SECRET_KEY, 'utf-8'))
    plaintext = arc4.decrypt(ciphertext)
    return (plaintext[3:3+int(plaintext[:3].strip())]).decode('utf-8')

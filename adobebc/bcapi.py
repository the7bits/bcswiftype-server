import requests


class BCAPI:
    def __init__(
            self, access_token=None, site_id=None,
            app_key=None):
        self.access_token = access_token
        self.site_id = site_id
        self.app_key = app_key
        self.api_host = 'https://%s-%s-apps.worldsecuresystems.com' % (
            app_key,
            site_id,
        )
        self.api_v3_root = self.api_host + '/webresources/api/v3'
        self.api_v2_root = self.api_host + '/api/v2/admin/sites/%s' % site_id

    def _get_v2_headers(self, content_type="application/octet-stream"):
        _headers = {
            "Authorization": self.access_token,
            "Content-Type": content_type,
        }
        return _headers

    def upload_file(
            self, filename='unknown.txt',
            content='empty', version='draft-publish'):
        _url = self.api_v2_root + '/storage/' + \
            filename + '?version=' + version
        _headers = self._get_v2_headers()
        response = requests.put(
            _url,
            headers=_headers,
            data=content,
        )
        return response

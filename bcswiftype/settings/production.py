# Settings for production environment (Grape)
from .base import *

DEBUG = False

ALLOWED_HOSTS = (
    'bcswiftypeapp.the7bits.com',
)

RAVEN_CONFIG = {
    'dsn': os.environ['SENTRY_DSN'],
}

INSTALLED_APPS = INSTALLED_APPS + (
    'raven.contrib.django.raven_compat',
)

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025

MEDIA_ROOT = '/home/app/webapp/public/media'
STATIC_ROOT = '/home/app/webapp/public/static'

# Basic settings for BC Swiftype Integration backend
import os
import dj_database_url
from django.utils.crypto import get_random_string

gettext = lambda s: s
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))


SECRET_KEY = os.environ.get(
    "SECRET_KEY",
    get_random_string(
        50, "abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)"))

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'crispy_forms',
    'easy_select2',

    'apps.core',
    'apps.engines',
    'apps.bcsites',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    # 'apps.bcsites.middleware.AutoBCLoginMiddleware',
    'apps.bcsites.middleware.LoginRequiredMiddleware',
)

ROOT_URLCONF = 'bcswiftype.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'bcswiftype', 'templates')
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'bcswiftype.wsgi.application'


# Database
DATABASES = {
    'default': dj_database_url.config(default=os.environ['DATABASE_URL']),
}


# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

LANGUAGES = (
    ('en', gettext('en'))
)


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

STATIC_URL = '/static/'
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'bcswiftype', 'static'),
)

# AUTH_USER_MODEL = 'accounts.User'
LOGIN_REDIRECT_URL = '/'
LOGIN_URL = '/login/'

CRISPY_TEMPLATE_PACK = 'bootstrap3'

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025

SELECT2_USE_BUNDLED_JQUERY = False

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'apps.bcsites.backends.BCSiteBackend',
)
LOGIN_EXEMPT_URLS = [
    '^$',
    '^admin/',
    '^bc-authorize',
    '^oauth-callback',
]

MAIN_PAGE_URL = '/engines/'

# BC app options
BC_OAUTH_AUTHORIZE_URL = \
    'https://ava-next.worldsecuresystems.com/api/oauth/authorize'
BC_OAUTH_TOKEN_URL = \
    'https://ava-next.worldsecuresystems.com/api/oauth/token'
BC_CLIENT_ID = os.environ['BC_CLIENT_ID']
BC_CLIENT_SECRET = os.environ['BC_CLIENT_SECRET']
BC_APP_VERSION = os.environ['BC_APP_VERSION']
BC_REDIRECT_URI = os.environ['BC_REDIRECT_URI']
BC_STATE = 'check'

ENCRYPT_SECRET_KEY = SECRET_KEY
BC_APP_DIR = '_System/apps/' + BC_CLIENT_ID

from django.db import models
from django.utils.translation import ugettext_lazy as _
from apps.bcsites.models import BCSite


class SwiftypeOptions(models.Model):
    bcsite = models.OneToOneField(
        BCSite,
        on_delete=models.CASCADE,
        verbose_name=_('BC Site'),
        related_name='swiftype_options',
    )
    api_key = models.CharField(
        _('API Key'),
        max_length=255,
    )

    class Meta:
        verbose_name = _('Swiftype Options')
        verbose_name_plural = _('Swiftype Options')

    def __str__(self):
        return str(self.bcsite)


class Engine(models.Model):
    bcsite = models.ForeignKey(
        BCSite,
        on_delete=models.CASCADE,
        verbose_name=_('BC Site'),
        related_name='swiftype_engines',
    )
    engine_id = models.CharField(_('Engine ID'), max_length=100)
    name = models.CharField(_('Name'), max_length=100)
    slug = models.CharField(_('Slug'), max_length=100)
    engine_key = models.CharField(
        _('Engine key'), max_length=100, unique=True)

    class Meta:
        verbose_name = _('Engine')
        verbose_name_plural = _('Engines')

    def __str__(self):
        return self.name

import binascii
from django.shortcuts import render, redirect
from django.conf import settings


def index(request):
    from apps.bcsites.utils import encrypt_string

    _set_cookie = False
    if 'HTTP_REFERER' in request.META:
        referer_url = request.META['HTTP_REFERER']
        if '.worldsecuresystems.com' in referer_url:
            referer_url = binascii.b2a_base64(encrypt_string(referer_url))
            _set_cookie = True

    if request.user.is_authenticated():
        response = redirect(settings.MAIN_PAGE_URL)
    else:
        response = render(request, 'index.html', {
            'inside_bcadmin': _set_cookie,
        })

    if _set_cookie:
        response.set_cookie('bcapp-referer', referer_url)

    return response


def help(request):
    return render(request, 'help.html', {})

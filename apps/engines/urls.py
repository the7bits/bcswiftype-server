from django.conf.urls import url
from .views import (
    EnginesListView,
    swiftype_options,
    add_existing_engine,
    engine_view,
    generate_search_widget,
)


urlpatterns = [
    url('^engines/$', EnginesListView.as_view(), name='engines-list'),
    url(
        '^engines/(?P<engine_key>[\d\w]+)/$',
        engine_view, name='engine-details'),
    url(
        '^engines/(?P<engine_key>[\d\w]+)/save-search-widget/$',
        generate_search_widget, name='engine-save-search-widget'),
    url(
        '^engines/add-existing/$',
        add_existing_engine,
        name='engines-add-existing'),
    url('^options/$', swiftype_options, name='swiftype-options'),
]

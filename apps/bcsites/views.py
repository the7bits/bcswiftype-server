from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _
from django.conf import settings


def bc_authorize(request):
    import urllib
    auth_url = '{0}?{1}'.format(
        settings.BC_OAUTH_AUTHORIZE_URL,
        urllib.parse.urlencode({
            'response_type': 'code',
            'client_id': settings.BC_CLIENT_ID,
            'version': settings.BC_APP_VERSION,
            'redirect_uri': settings.BC_REDIRECT_URI,
            'state': settings.BC_STATE,
        })
    )
    return render(request, 'bcsites/bc_authorize.html', {
        'auth_url': auth_url,
    })


def _request_a_token(code):
    import requests
    _data = {
        'grant_type': 'authorization_code',
        'redirect_uri': settings.BC_REDIRECT_URI,
        'client_secret': settings.BC_CLIENT_SECRET,
        'code': code,
        'client_id': settings.BC_CLIENT_ID,
    }
    _headers = {
        "Accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded",
    }
    r = requests.post(
        settings.BC_OAUTH_TOKEN_URL,
        data=_data,
        headers=_headers,
    )
    return r.json()


def oauth_callback(request):
    from .utils import update_bc_sites_db
    _restrict = True

    client_id = request.GET.get('client_id')
    redirect_uri = request.GET.get('redirect_uri')
    version = request.GET.get('version')
    response_type = request.GET.get('response_type')
    code = request.GET.get('code')

    _step2 = ''

    if client_id:
        _restrict = False
        _step2 = _request_a_token(code)
        _restrict = not update_bc_sites_db(_step2)

    return render(request, 'bcsites/bc_callback.html', {
        'restrict': _restrict,
        'client_id': client_id,
        'redirect_uri': redirect_uri,
        'version': version,
        'response_type': response_type,
        'code': code,
        'step2': _step2,
    })


def login(request):
    import binascii
    from django.contrib.auth import authenticate, login
    from apps.bcsites.utils import decrypt_cipher
    if not request.user.is_authenticated():
        if 'bcapp-referer' in request.COOKIES:
            encrypted_referer = \
                binascii.a2b_base64(request.COOKIES['bcapp-referer'])
            referer_url = decrypt_cipher(encrypted_referer)
            user = authenticate(referer_url=referer_url)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect(settings.MAIN_PAGE_URL)
            return render(request, 'bcsites/incorrect_login.html', {
                'error_message':
                _('Cannot find requested user.'),
            })
        else:
            return render(request, 'bcsites/incorrect_login.html', {
                'error_message':
                _('You may use this application only '
                    'inside BC admin console.'),
            })
    else:
        return redirect(settings.MAIN_PAGE_URL)

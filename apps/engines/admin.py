from django.contrib import admin
from .models import (
    SwiftypeOptions,
    Engine,
)


class SwiftypeOptionsAdmin(admin.ModelAdmin):
    list_display = ('bcsite', 'api_key')


class EngineAdmin(admin.ModelAdmin):
    list_display = ('bcsite', 'name', 'engine_key')

admin.site.register(SwiftypeOptions, SwiftypeOptionsAdmin)
admin.site.register(Engine, EngineAdmin)

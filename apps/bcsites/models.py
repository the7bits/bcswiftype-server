from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from datetime import timedelta


class BCPartner(models.Model):
    partner_id = models.IntegerField(_('Partner ID'), unique=True)
    access_token = models.TextField(_('Access token'))
    refresh_token = models.TextField(_('Refresh token'))
    token_type = models.CharField(
        _('Token type'), max_length=30, default='bearer')

    added = models.DateTimeField(
        _('Added'), auto_now_add=True)
    last_update = models.DateTimeField(
        _('Last update'), auto_now=True)
    access_token_expires = models.DateTimeField(
        _('Access token expires'))
    refresh_token_expires = models.DateTimeField(
        _('Refresh token expires'))

    class Meta:
        verbose_name = _('BC Partner')
        verbose_name_plural = _('BC Partners')

    def __str__(self):
        return str(self.partner_id)

    def is_access_token_expired(self):
        return timezone.now() >= self.access_token_expires

    def is_refresh_token_expired(self):
        return timezone.now() >= self.refresh_token_expires

    def refresh_access_token(self):
        import requests
        from .utils import update_bc_sites_db
        if not self.is_refresh_token_expired():
            _data = {
                'grant_type': 'refresh_token',
                'redirect_uri': settings.BC_REDIRECT_URI,
                'client_secret': settings.BC_CLIENT_SECRET,
                'refresh_token': self.refresh_token,
                'client_id': settings.BC_CLIENT_ID,
            }
            _headers = {
                "Accept": "application/json",
                "Content-Type": "application/x-www-form-urlencoded",
            }
            r = requests.post(
                settings.BC_OAUTH_TOKEN_URL,
                data=_data,
                headers=_headers,
            )
            result = r.json()
            if update_bc_sites_db(result) is True:
                return result['access_token']
        else:
            return None


class BCSite(models.Model):
    name = models.CharField(_('Name'), max_length=255)
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        verbose_name=_('User'),
        related_name='bcsite',
    )   # We use different users for different sites
    partner = models.ForeignKey(
        BCPartner,
        on_delete=models.PROTECT,
        verbose_name=_('BC Partner'),
        related_name='bcsites')
    site_id = models.IntegerField(_('Site ID'), unique=True)
    secure_url = models.CharField(
        _('Secure URL'), max_length=255, unique=True)
    added = models.DateTimeField(
        _('Added'), auto_now_add=True)
    last_update = models.DateTimeField(
        _('Last update'), auto_now=True)

    class Meta:
        verbose_name = _('BC Site')
        verbose_name_plural = _('BC Sites')

    def __str__(self):
        return self.secure_url

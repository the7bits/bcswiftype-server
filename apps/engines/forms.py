from django import forms
from django.utils.translation import ugettext_lazy as _
from .models import SwiftypeOptions


class SwiftypeOptionsForm(forms.ModelForm):
    class Meta:
        model = SwiftypeOptions
        fields = ['api_key']


class SelectExistingEngine(forms.Form):
    engine_name = forms.ChoiceField(
        label=_('Select an engine'))

    def __init__(self, engines, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['engine_name'].choices = \
            [(e['slug'], e['name']) for e in engines]

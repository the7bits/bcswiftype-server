from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView
from django.conf import settings
from .models import Engine
from .forms import (
    SwiftypeOptionsForm,
    SelectExistingEngine,
)


@login_required
def swiftype_options(request):
    user = request.user
    bcsite = user.bcsite

    if request.method == 'POST':
        if hasattr(bcsite, 'swiftype_options'):
            form = SwiftypeOptionsForm(
                request.POST, instance=bcsite.swiftype_options)
        else:
            form = SwiftypeOptionsForm(request.POST)
        if form.is_valid():
            options = form.save(commit=False)
            options.bcsite = bcsite
            options.save()
    else:
        if hasattr(bcsite, 'swiftype_options'):
            form = SwiftypeOptionsForm(instance=bcsite.swiftype_options)
        else:
            form = SwiftypeOptionsForm()

    return render(request, 'engines/swiftype_options.html', {
        'form': form,
    })


@login_required
def add_existing_engine(request):
    import requests

    user = request.user
    bcsite = user.bcsite
    options = bcsite.swiftype_options

    _params = {
        'auth_token': options.api_key,
    }
    _headers = {
        "Content-Type": "application/json",
    }

    r = requests.get(
        'https://api.swiftype.com/api/v1/engines.json',
        params=_params,
        headers=_headers,
    )

    engines = r.json()

    if request.method == 'POST':
        form = SelectExistingEngine(engines, request.POST)
        if form.is_valid():
            # Get an info about existing engine
            r = requests.get(
                'https://api.swiftype.com/api/v1/engines/%s.json' %
                form.cleaned_data['engine_name'],
                params=_params,
                headers=_headers,
            )
            engine_info = r.json()

            try:
                Engine.objects.get(
                    bcsite=bcsite, slug__exact=engine_info['slug'])
            except Engine.DoesNotExist:
                _engine = Engine(
                    bcsite=bcsite,
                    engine_id=engine_info['id'],
                    slug=engine_info['slug'],
                    engine_key=engine_info['key'],
                    name=engine_info['name'],
                )
                _engine.save()
            return redirect(reverse('engines-list'))
    else:
        form = SelectExistingEngine(engines)

    return render(request, 'engines/add_existing_engine.html', {
        'form': form,
    })


@login_required
def engine_view(request, engine_key):
    engine = get_object_or_404(
        Engine,
        engine_key__exact=engine_key,
        bcsite__user=request.user)
    return render(request, 'engines/engine_details.html', {
        'engine': engine,
    })


@login_required
def generate_search_widget(request, engine_key):
    from adobebc import BCAPI
    from apps.bcsites.utils import get_access_token
    user = request.user
    engine = get_object_or_404(
        Engine,
        engine_key__exact=engine_key,
        bcsite__user=request.user)

    _content = render_to_string(
        'engines/_search_widget.html',
        context={'engine': engine},
        request=request,
    )

    site_id = user.username
    access_token = get_access_token(site_id=site_id)
    bcapi = BCAPI(
        access_token=access_token, site_id=site_id,
        app_key=settings.BC_CLIENT_ID)
    bcapi.upload_file(
        filename=
        settings.BC_APP_DIR + '/search-widget--%s.inc' % engine.slug,
        content=_content)
    return redirect(reverse('engine-details', args=[engine_key]))


class EnginesListView(ListView):
    model = Engine

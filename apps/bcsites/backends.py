import re
from django.conf import settings
from django.contrib.auth import get_user_model


class BCSiteBackend(object):
    def authenticate(self, referer_url=None):
        if referer_url is None:
            return None

        p = r'.*' + \
            settings.BC_CLIENT_ID + \
            r'-(?P<site_id>\d+)-apps.worldsecuresystems.com.*'
        try:
            site_id = re.search(p, referer_url).group(1)
        except:
            site_id = None

        User = get_user_model()

        if site_id is not None:
            try:
                user = User.objects.get(username__exact=str(site_id))
                return user
            except User.DoesNotExist:
                return None
        return None

    def get_user(self, user_id):
        User = get_user_model()

        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None

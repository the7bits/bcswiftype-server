from django.conf.urls import url
from .views import (
    bc_authorize,
    oauth_callback,
    login,
)


urlpatterns = [
    url(r'^login/$', login),
    url(r'^bc-authorize$', bc_authorize, name="bc-authorize"),
    url(r'^oauth-callback$', oauth_callback),
]
